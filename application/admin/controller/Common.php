<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-12-27
 * Time: 16:02
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;
use think\Image;

class Common extends Controller {

    //分页步进数
    protected $step = 10;

    //优先执行
    protected function _initialize(){
        //登录限制
        $this->isLogin();

        //菜单
        if (!session('menu')) {
            $this->get_menu();
        }
    }

    public function isLogin(){
        if(!session('admin')){
            $this->redirect('/admin/login/');
            exit;
        }
    }
    
    //获取管理员id
    protected function admin_id()
    {   

        return session('admin')['id'];
    }

    //上传图片
    public function uploadfile($filename,$path){

        $file = request()->file($filename);

        if (is_array($file) && count($file) > 0) {


            $images = array();

            foreach($file as $key => $files){
                // 移动到框架应用根目录/public/uploads/ 目录下
                $info = $files->validate(['size'=>22428800,'ext'=>'jpg,png,gif,jpeg'])->move($path);
                if($info){
                    // 成功上传后 获取上传信息
                    $get_path = $path.$info->getSaveName();
            
                    $get_path=str_replace("\\","/",$get_path);

                    $images[] = $get_path;

                }else{
                    // 上传失败获取错误信息
                    return array('error'=>1,'msg'=>'第 '.($key + 1).' 张图上传失败，'.$files->getError());
                }    
            }

            return array('error'=>0,'path'=>$images);
        }else{

            $info = $file->validate(['size'=>22428800,'ext'=>'jpg,png,gif,jpeg'])->move($path);
        
            if($info){
            
                $path .= $info->getSaveName();
            
                $path=str_replace("\\","/",$path);
            
                return array('error'=>0,'path'=>$path);
            
            }else{
            
                return array('error'=>1,'msg'=>$file->getError());
            }
        }
        
    }

    //获取登录者的角色菜单
    public function get_menu(){

        $group_id = session('admin')['auth_group_id'];

        if(empty($group_id)){
            
            echo '<head><title>管理后台</title></head><div style="width:100%;height:300px;text-align:center;line-height:300px;font-size:30px;"> 您未分配职位，请联系管理员！ </div>';
            exit;
        }

        $menu = Db::table('admin_menu');

        $group_res = Db::table('admin_auth_group')->where(array('auth_group_id' => $group_id))->find();

        if(empty($group_res)){

            echo '<head><title>管理后台</title></head><div style="width:100%;height:300px;text-align:center;line-height:300px;font-size:30px;"> 您的职位已被删除，请联系管理员，重新分配职位！ </div>';
            exit;
        }

        if($group_res['status'] != 1){

            echo '<head><title>管理后台</title></head><div style="width:100%;height:300px;text-align:center;line-height:300px;font-size:30px;"> 职位：“'.$group_res['group_name'].'” 已被禁用，请联系管理员！ </div>';
            exit;
        }

        $menu_ids = $group_res['menus'];

        if(empty($menu_ids) && session('admin')['id'] != 1){

            echo '<head><title>管理后台</title></head><div style="width:100%;height:300px;text-align:center;line-height:300px;font-size:30px;"> 职位：“'.$group_res['group_name'].'” 没有任何可查看菜单，请联系管理员！ </div>';
            exit;
        }

        session('admin')['group_name'] = $group_res['group_name'];

        if(session('admin')['id'] == 1){

            $menus = Db::table('admin_menu')->where('status=1')->order('pid ASC,seq DESC')->select();

        }else{

            $first_menus = Db::table('admin_menu')->field('pid')->where('status=1 and menu_id in ('.$menu_ids.')')->select();
            $all_menu_ids = implode(',',array_column($first_menus, 'pid')).','.$menu_ids;
            $menus = Db::table('admin_menu')->where('status=1 and menu_id in ('.$all_menu_ids.')')->order('pid ASC,seq DESC')->select();
        }
        
        $new_menus = array();

        foreach($menus as $key => $value){

            if ($value['pid'] == 0) {

                if (!isset($new_menus[$value['menu_id']])) {
                    $new_menus[$value['menu_id']] = array();
                }

                $new_menus[$value['menu_id']] = array_merge($value,$new_menus[$value['menu_id']]);

            }else{
                $new_menus[$value['pid']]['child'][] = $value;
            }

        }

        session('menu',$new_menus);
    }

    //记录操作日志
    public function action_log($content,$type = 2){

        $data['admin_id'] = session('admin')['id'];
        $data['action_content'] = $content;
        $data['type'] = $type;
        $data['log_time'] = date('Y-m-d H:i:s');

        Db::table('admin_action_log')->insert($data);
    }

    //将对象转换为数组
    public function objToarray($obj = array()){

        return json_decode(json_encode($obj),1);
    }
}
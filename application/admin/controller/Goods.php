<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;

class Goods extends Common {
	//商品列表
    public function lists(){

        $where = array('is_del' => 0);

        $step = 10;

        if(isset($_GET['keywords'])){
            $where['goods_name'] = array('like','%'.$_GET['keywords'].'%');
        }

        $count = Db::table('goods')->where($where)->count();

        $res = Db::table('goods')->where($where)->order('add_time desc')->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $this->assign(['res' => $this->objToarray($res)['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);

        return $this->fetch();
    }

    //添加商品页面
    public function add(){

        if(Request::instance()->isGet()){
            $goods_type = Db::table('goods_type')->field('type_id,type_name')->where(array('is_del' => 0))->select();

            $this->assign('goods_type',$goods_type);
            return $this->fetch();

        }else{

            if((!empty($_POST['goods_name'])) && (!empty($_POST['describe'])) && (intval($_POST['type_id']) >= 0)){

                $path = './uploads/goods/';
             
                if($_FILES['image_first']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('image_first',$path);
                	
                	if ($info['error'] == 1) {
                		
                		echo (json_encode(['error' => 1,'msg' => '封面图上传失败，'.$info['msg']]));exit;
                	}else{

                    	$data['goods_img'] = substr($info['path'],1);
                	}
                }
				
				if($_FILES['goods_info']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('goods_info',$path);
                	
                	if ($info['error'] == 1) {
                		
                		echo (json_encode(['error' => 1,'msg' => '详情图上传失败，'.$info['msg']]));exit;
                	}else{

                    	$data['goods_info'] = substr($info['path'],1);
                	}
                }

                $images = array();

                for ($i = 1; $i <= 6; $i++) { 
                	
                	if($_FILES['image'.$i]['error'] == 0){
	                    //上传图片
	                    $info = $this->uploadfile('image'.$i,$path);
	                	
	                	if ($info['error'] == 1) {
	                		
	                		echo (json_encode(['error' => 1,'msg' => '商品图 '.$i.' 上传失败，'.$info['msg']]));exit;
	                	}else{

	                		$get_path = substr($info['path'],1);

	                    	$images[$i] = $get_path;
	                	}
	                }else{

	                	$images[$i] = '';
	                }
                }

                $data['goods_imgs'] = implode(';',$images);
                $data['goods_name'] = $_POST['goods_name'];
                $data['describe'] = $_POST['describe'];
                $data['goods_num'] = $_POST['goods_num'];
                $data['price'] = $_POST['price'];
                $data['original_price'] = $_POST['original_price'];
                $data['p_return'] = $_POST['p_return'];
                $data['gp_return'] = $_POST['gp_return'];
                $data['is_show_home'] = $_POST['is_show_home'];
                $data['type_id'] = $_POST['type_id'];
                $data['is_del'] = 0;
                $data['add_time'] = date('Y-m-d H:i:s');

                $bool = Db::table('goods')->insertGetId($data);

                if($bool > 0){

                    //记录log日志
                    $this->action_log('添加了商品，ID#'.$bool);

                    echo (json_encode(['error' => 0,'msg' => '添加成功']));exit;

                }else{

                    echo (json_encode(['error' => 1,'msg' => '添加失败']));exit;
                }

            }else{

                echo (json_encode(['error' => 1,'msg' => '表单内容没有补全']));exit;
            }
        }
    }

    //编辑商品页面
    public function edit() {

        if(Request::instance()->isGet()){

            $goods_id = input('get.goods_id');

            $res = Db::table('goods')->find($goods_id);

            if(!$res) die('参数错误');
            
            $types = Db::table('goods_type')->field('type_id,type_name')->where(array('is_del' => 0))->select();
            $res['goods_imgs'] = explode(';',$res['goods_imgs']);
            $this->assign('goods_imgs',$res['goods_imgs']);
            $this->assign('types',$types);
            $this->assign('res',$res);
            return $this->fetch();

        }else{

        	if (!isset($_POST['goods_id']) || empty($_POST['goods_id'])) {
        		
                echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        	}

        	$goods_res = Db::table('goods')->where(['goods_id' => $_POST['goods_id']])->find();

        	$data['goods_img'] = $goods_res['goods_img'];
        	$data['goods_info'] = $goods_res['goods_info'];
        	$images = explode(';',$goods_res['goods_imgs']);

            if((!empty($_POST['goods_name'])) && (!empty($_POST['describe'])) && (intval($_POST['type_id']) >= 0)){

                $path = './uploads/goods/';

                if($_FILES['image_first']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('image_first',$path);
                	
                	if ($info['error'] == 1) {
                		
                		echo (json_encode(['error' => 1,'msg' => '封面图上传失败，'.$info['msg']]));exit;
                	}else{

                    	$data['goods_img'] = substr($info['path'],1);
                	}
                }
				
				if($_FILES['goods_info']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('goods_info',$path);
                	
                	if ($info['error'] == 1) {
                		
                		echo (json_encode(['error' => 1,'msg' => '详情图上传失败，'.$info['msg']]));exit;
                	}else{

                    	$data['goods_info'] = substr($info['path'],1);
                	}
                }

                for ($i = 1; $i <= 6; $i++) { 
                	
                	if($_FILES['image'.$i]['error'] == 0){
	                    //上传图片
	                    $info = $this->uploadfile('image'.$i,$path);
	                	
	                	if ($info['error'] == 1) {
	                		
	                		echo (json_encode(['error' => 1,'msg' => '商品图 '.$i.' 上传失败，'.$info['msg']]));exit;
	                	}else{

	                		$get_path = substr($info['path'],1);

	                    	$images[$i-1] = $get_path;
	                	}
	                }
                }

                $data['goods_imgs'] = implode(';',$images);
                $data['goods_name'] = $_POST['goods_name'];
                $data['describe'] = $_POST['describe'];
                $data['goods_num'] = $_POST['goods_num'];
                $data['price'] = $_POST['price'];
                $data['original_price'] = $_POST['original_price'];
                $data['p_return'] = $_POST['p_return'];
                $data['gp_return'] = $_POST['gp_return'];
                $data['is_show_home'] = $_POST['is_show_home'];
                $data['type_id'] = $_POST['type_id'];

                $bool = Db::table('goods')->where(['goods_id' => $_POST['goods_id']])->update($data);

                if($bool > 0) {

                    //记录log日志
                    $this->action_log('编辑了商品内容，ID'.$_POST['goods_id'].'为#'.serialize($data));

                    echo (json_encode(['error' => 0,'msg' => '修改成功']));exit;

                }else{

                    echo (json_encode(['error' => 1,'msg' => '修改失败']));exit;
                }

            }else{

                echo (json_encode(['error' => 1,'msg' => '文件错误或内容没填全']));exit;
            }
        }
    }

    //删除商品
    public function delete(){

        if(!isset($_POST['goods_id']) || empty($_POST['goods_id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $goods_id = $_POST['goods_id'];

        $bool = Db::table('goods')->where(array('goods_id' => $goods_id))->update(array('is_del' => 1));

        if($bool){

            //记录log日志
            $this->action_log('删除了商品，ID#'.$goods_id);

            echo (json_encode(['error' => 0,'msg' => '删除成功']));exit;

        }else{

            echo (json_encode(['error' => 1,'msg' => '删除失败']));exit;
        }
    }

    //商品分类列表
    public function type_list(){

        $where = array();

        $step = 20;

        if(isset($_GET['keywords'])){
        
            $where['type_name'] = array('like','%'.$_GET['keywords'].'%');
        }

        if(isset($_GET['status']) && $_GET['status'] != 'all'){

            $where['is_del'] = $_GET['status'];
        }

        $count = Db::table('goods_type')->where($where)->count();

        $res = Db::table('goods_type')->where($where)->order('seq asc,create_time desc')->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $this->assign(['res' => $this->objToarray($res)['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);

        return $this->fetch();
    }

    //添加商品分类页面
    public function type_add(){

        if(Request::instance()->isGet()){

            return $this->fetch();

        }else{

            if(!empty($_POST['type_name'])){

                $isset = Db::table('goods_type')->where(['type_name' => $_POST['type_name']])->find();
                
                if ($isset) {

                    echo (json_encode(['error' => 1,'msg' => '分类名称已存在']));exit;
                }

                $path = './uploads/goods_type/';
                
                if($_FILES['type_img']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('type_img',$path);
                    
                    if ($info['error'] == 1) {
                        
                        echo (json_encode(['error' => 1,'msg' => '分类图片上传失败，'.$info['msg']]));exit;
                    }else{

                        $data['type_img'] = substr($info['path'],1);
                    }
                }

    
                if (isset($_POST['seq']) && !empty($_POST['seq'])) {
                    
                    $data['seq'] = $_POST['seq'];
                }
    
                $data['type_name'] = $_POST['type_name'];
                $data['is_del'] = 0;
                $data['create_time'] = date('Y-m-d H:i:s');

                $bool = Db::table('goods_type')->insertGetId($data);

                if($bool > 0){

                    //记录log日志
                    $this->action_log('添加了商品分类，ID#'.$bool);

                    echo (json_encode(['error' => 0,'msg' => '添加成功']));exit;

                }else{

                    echo (json_encode(['error' => 1,'msg' => '添加失败']));exit;
                }

            }else{

                echo (json_encode(['error' => 1,'msg' => '表单内容没有补全']));exit;
            }
        }
    }

    //编辑商品分类页面
    public function type_edit(){

        if(Request::instance()->isGet()){

            if(!isset($_GET['type_id']) || empty($_GET['type_id'])){

                echo '<script>alert("参数错误");history.back(-1);</script>';exit;
            }

            $res = Db::table('goods_type')->where(['type_id' => $_GET['type_id']])->find();

            $this->assign('res',$res);
            return $this->fetch();

        }else{
            
            if (empty($_POST['type_id'])) {
                
                echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
            }

            if(!empty($_POST['type_name'])){

                $isset = Db::table('goods_type')->where('type_name = "'.$_POST['type_name'].'" and type_id !='.$_POST['type_id'])->find();
                
                if ($isset) {

                    echo (json_encode(['error' => 1,'msg' => '分类名称已存在']));exit;
                }

                $path = './uploads/goods_type/';
                
                if($_FILES['type_img']['error'] == 0){
                    //上传图片
                    $info = $this->uploadfile('type_img',$path);
                    
                    if ($info['error'] == 1) {
                        
                        echo (json_encode(['error' => 1,'msg' => '分类图片上传失败，'.$info['msg']]));exit;
                    }else{

                        $data['type_img'] = substr($info['path'],1);
                    }                    
                }

    
                if (isset($_POST['seq']) && !empty($_POST['seq'])) {
                    
                    $data['seq'] = $_POST['seq'];
                }
    
                $data['type_name'] = $_POST['type_name'];
                $data['is_del'] = 0;
                $data['create_time'] = date('Y-m-d H:i:s');

                $bool = Db::table('goods_type')->where(['type_id' => $_POST['type_id']])->update($data);

                if($bool > 0){

                    //记录log日志
                    $this->action_log('修改了商品分类，ID#'.$bool);

                    echo (json_encode(['error' => 0,'msg' => '修改成功']));exit;

                }else{

                    echo (json_encode(['error' => 1,'msg' => '修改失败']));exit;
                }

            }else{

                echo (json_encode(['error' => 1,'msg' => '表单内容没有补全']));exit;
            }
        }
    }

    //删除商品分类
    public function type_delete(){

        if(!isset($_POST['type_id']) || empty($_POST['type_id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $type_id = $_POST['type_id'];

        if ($_POST['status'] == 1) {
            
            $is_del = 0;
        
        }else{

            $is_del = 1;
        }

        $bool = Db::table('goods_type')->where(array('type_id' => $type_id))->update(array('is_del' => $is_del));

        if($bool){

            //记录log日志
            $this->action_log('禁用了商品分类，ID#'.$type_id);

            echo (json_encode(['error' => 0,'msg' => '禁用成功']));exit;

        }else{

            echo (json_encode(['error' => 1,'msg' => '禁用失败']));exit;
        }
    }
}
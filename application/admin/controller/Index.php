<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-11-27
 * Time: 16:02
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;

class Index extends Common {

    //首页
    public function index() {
        
        $info = Db::table('admin')->find($this->admin_id());
        $this->assign(['info' => $info]);
        return $this->fetch();
    }

    //后台默认缺省页
    public function index_base()
    {
        return $this->fetch();
    }

}

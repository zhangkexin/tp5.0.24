<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-11-27
 * Time: 16:02
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;
use app\admin\controller\Common;
class Login extends Controller {

	//登录
    public function index(){

        if(Request::instance()->isPost()){
        
            $username = input('post.username');
        
            $password = input('post.password');

            $res = Db::table('admin')->where(array('username' => $username,'password' => md5($password)))->find();
            
            if($res){
            	if($res['status'] != 1){
                    echo json_encode(array('error' => 1,'msg' => '登录失败，您的账号已被禁用'));exit;
                }

                $res['group_name'] = Db::table('admin_auth_group')->where(['auth_group_id' => $res['auth_group_id']])->value('group_name');
                
                session('admin',$res);
                
                //记录log日志
                $data['admin_id'] = session('admin')['id'];
                $data['action_content'] = '登录了网站后台';
                $data['type'] = 1;
                $data['log_time'] = date('Y-m-d H:i:s');

                Db::table('admin_action_log')->insert($data);

                echo json_encode(array('error' => 0,'msg' => '登录成功'));exit;
            
            }else{
            
                echo json_encode(array('error' => 1,'msg' => '登录失败，账号或密码错误'));exit;
            
            }
        }

        return $this->fetch();
    }

    //退出
    public function logout(){
        session('admin',null);
        session('menu',null);
        $this->redirect('/admin/login/');
    }

}

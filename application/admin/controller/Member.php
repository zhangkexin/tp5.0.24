<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;

class Member extends Common {

	//用户列表
	public function member_list(){

		$where = '1=1';

        if(isset($_GET['user_name']) && !empty($_GET['user_name'])){

            $where .= ' and user_name like "%'.$_GET['user_name'].'%"';
        }

        $count = Db::table('user')->where($where)->count();

        $step = 10;

        $res = Db::table('user')->where($where)->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);
        
        $this->assign(['res' => $this->objToarray($res)['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);
		return $this->fetch();
	}

    //提现审核列表
    public function tx_auditing_list(){

        $where = '1=1';

        if(isset($_GET['user_name']) && !empty($_GET['user_name'])){

            $where .= ' and u.user_name like "%'.$_GET['user_name'].'%"';
        }

        if(isset($_GET['status']) && $_GET['status'] != 'all'){

            $where .= ' and ut.type = '.$_GET['status'].'';
        }

        $count = Db::table('user_tx')->alias('ut')->join('user u','u.user_id = ut.user_id','LEFT')->where($where)->count();

        $step = 10;

        $res = Db::table('user_tx')->alias('ut')->join('user u','u.user_id = ut.user_id','LEFT')->where($where)->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $types = ['0' => '未审核','1' => '<span class="text-success">已通过</span>','2' => '<span class="text-danger">已拒绝</span>'];

        foreach ($new_res['data'] as $key => $value) {
            
            $new_res['data'][$key]['str_type'] = $types[$value['type']];
        }

        $this->assign(['res' => $new_res['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);
        return $this->fetch();
    }

    //通过审核
    public function tx_auditing_pass(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $id = $_POST['id'];

        $bool = Db::table('user_tx')->where(array('id' => $id))->update(array('type' => 1));

        if($bool){

            //记录log日志
            $this->action_log('通过提现记录，ID#'.$id);

            echo (json_encode(['error' => 0,'msg' => '已通过审核']));exit;

        }else{

            echo (json_encode(['error' => 1,'msg' => '通过审核失败']));exit;
        }
    }

    //拒绝审核
    public function tx_auditing_refuse(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $id = $_POST['id'];

        $bool = Db::table('user_tx')->where(array('id' => $id))->update(array('type' => 2));

        if($bool){

            //记录log日志
            $this->action_log('拒绝提现记录，ID#'.$id);

            echo (json_encode(['error' => 0,'msg' => '已拒绝审核']));exit;

        }else{

            echo (json_encode(['error' => 1,'msg' => '拒绝审核失败']));exit;
        }
    }

    //添加提现备注
    public function tx_remark(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        if(!isset($_POST['remark']) || empty($_POST['remark'])){

            echo (json_encode(['error' => 1,'msg' => '备注不能为空']));exit;
        }

        $id = $_POST['id'];

        $result = Db::table('user_tx')->where(['id' => $id])->update(['note' => $_POST['remark']]);

        if (is_int($result)) {
            
            echo (json_encode(['error' => 0,'msg' => '添加备注成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '添加备注失败']));exit;
        }
    }

     //用户建议列表
    public function suggest_list(){

        $where = '1=1';

        if(isset($_GET['is_read']) && !empty($_GET['is_read'])){

                $where .= ' and is_read = '.($_GET['is_read'] - 1);
        }

        if(isset($_GET['phone']) && !empty($_GET['phone'])){

            $where .= ' and phone like "%'.$_GET['phone'].'%"';
        }

        $count = Db::table('user_suggest')->where($where)->count();

        $step = 10;

        $res = Db::table('user_suggest')->where($where)->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $this->assign(['res' => $new_res['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);
        return $this->fetch();
    }

    //用户建议处理
    public function suggest_read(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $id = $_POST['id'];

        $result = Db::table('user_suggest')->where(['id' => $id])->update(['is_read' => 1]);

        if ($result) {
            
            echo (json_encode(['error' => 0,'msg' => '标记为“已处理”成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '标记为“已处理”失败']));exit;
        }
    }

    //添加建议备注
    public function suggest_remark(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        if(!isset($_POST['remark']) || empty($_POST['remark'])){

            echo (json_encode(['error' => 1,'msg' => '备注不能为空']));exit;
        }

        $id = $_POST['id'];

        $result = Db::table('user_suggest')->where(['id' => $id])->update(['remark' => $_POST['remark']]);

        if (is_int($result)) {
            
            echo (json_encode(['error' => 0,'msg' => '添加备注成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '添加备注失败']));exit;
        }
    }
}

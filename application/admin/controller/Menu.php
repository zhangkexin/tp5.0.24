<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;

class Menu extends Common
{

    //菜单列表
    public function menu_list(){
        $menus = Db::table('admin_menu')->order('pid ASC,seq DESC')->select();
        $new_menus = array();
        foreach ($menus as $key => $value) {
            if ($value['pid'] == 0) {
                if (!isset($new_menus[$value['menu_id']])) {
                    $new_menus[$value['menu_id']] = array();
                }
                $new_menus[$value['menu_id']] = array_merge($value,$new_menus[$value['menu_id']]);
            }else{
                $new_menus[$value['pid']]['child'][] = $value;
            }
        }
        $last_menus = array();
        foreach ($new_menus as $k => $v) {
            if (isset($v['child'])) {
                $child = $v['child'];
                unset($v['child']);
                $last_menus[] = $v;
                foreach ($child as $key => $value) {
                    $last_menus[] = $value;
                }
            }else{
                $last_menus[] = $v;
            }
        }
        $this->assign(['last_menus' => $last_menus]);
        return $this->fetch();
    }

    //菜单添加
    public function menu_add(){
        if (Request::instance()->isPost()) {
            $post = $_POST;
            if (empty($post['menu_name'])) {
                echo json_encode(array('error' => 1,'msg'=>'请填写菜单名称'));exit; 
            }
            if (Db::table('admin_menu')->where(array('menu_name'=>$post['menu_name']))->count() > 0) {
                echo json_encode(array('error' => 1,'msg'=>'菜单名称已存在'));exit; 
            }
            if (empty($post['pid']) && $post['pid'] != 0) {
                echo json_encode(array('error' => 1,'msg'=>'请选择一级菜单'));exit; 
            }
            if (empty($post['menu_url'])) {
                $post['menu_url'] = '';
            }
            if (empty($post['status'])) {
                echo json_encode(array('error' => 1,'msg'=>'请选择菜单状态'));exit; 
            }

            $data = array(
                'menu_name' => preg_replace('# #','',$post['menu_name']),
                'pid' => preg_replace('# #','',$post['pid']),
                'menu_url' => preg_replace('# #','',$post['menu_url']),
                'ico' => preg_replace('# #','',$post['ico']),
                'status' => preg_replace('# #','',$post['status']) != 1?2:1,
                'seq' => $post['seq']
            );
            $result = Db::table('admin_menu')->insertGetId($data);
            if ($result) {
                //记录log日志
                $this->action_log('添加了菜单，ID#'.$result);
                echo json_encode(array('error' => 0,'msg' => '添加成功'));exit;
            }else{
                echo json_encode(array('error' => 1,'msg' => '添加失败'));exit;
            }
            
        }
        $where = array(
            'pid' => array('eq',0),
            'menu_url' => array('eq','')
        );
        $first_menu = Db::table('admin_menu')->where($where)->order('seq DESC')->select();
        $this->assign(['first_menu' => $first_menu]);
        return $this->fetch();
    }

    //菜单编辑
    public function menu_edit(){
        if (Request::instance()->isPost()) {
            $post = $_POST;
            if (empty($post['menu_id'])) {
                echo json_encode(array('error' => 1,'msg' => '参数错误'));exit; 
            }
            if (Db::table('admin_menu')->where(array('menu_name'=>$post['menu_name'],'menu_id' => array('neq',$post['menu_id'])))->count() > 0) {
                echo json_encode(array('error' => 1,'msg'=>'菜单名称已存在'));exit; 
            }
            if (isset($post['menu_name']) && empty($post['menu_name'])) {
                echo json_encode(array('error' => 1,'msg' => '请填写菜单名称'));exit; 
            }
            if (!isset($post['pid'])) {
                $post['pid'] = '';
            }
            if (empty($post['pid']) && $post['pid'] != 0) {
                echo json_encode(array('error' => 1,'msg' => '请选择一级菜单'));exit; 
            }
            if (!isset($post['menu_url'])) {
                $post['menu_url'] = '';
            }
            if (empty($post['menu_url'])) {
                $post['menu_url'] = '';
            }

            $data = array(
                'menu_name' => preg_replace('# #','',$post['menu_name']),
                'pid' => preg_replace('# #','',$post['pid']),
                'menu_url' => preg_replace('# #','',$post['menu_url']),
                'ico' => preg_replace('# #','',$post['ico']),
                'seq' => $post['seq']
            );
            $result = Db::table('admin_menu')->where(array('menu_id' => $post['menu_id']))->update($data);
            if ($result) {
                //记录log日志
                $this->action_log('修改了菜单，ID'.$post['menu_id'].'为#'.serialize($data));
                echo json_encode(array('error' => 0,'msg' => '修改成功'));exit;
            }else{
                echo json_encode(array('error' => 1,'msg' => '修改失败'));exit;
            }
            
        }
        if (empty($_GET['menu_id'])) {
            echo '<script>swal("参数错误", "", "success");setTimeout("self.location=document.referrer;",2000);</script>';exit;
        }
        $where = array(
            'pid' => array('eq',0),
            'menu_url' => array('eq','')
        );
        $first_menu = Db::table('admin_menu')->where($where)->order('seq DESC')->select();
        $self_menu = Db::table('admin_menu')->where(array('menu_id' => $_GET['menu_id']))->find();
        $child_menu = Db::table('admin_menu')->where(array('pid' => $_GET['menu_id']))->count();
        $this->assign(['first_menu' => $first_menu,'self_menu' => $self_menu,'child_menu' => $child_menu]);
        return $this->fetch();
    }

    //菜单删除
    public function menu_del(){
        if (Request::instance()->isPost()) {
            $post = $_POST;
            if (empty($post['menu_id'])) {
                echo json_encode(array('error' => 1,'msg' => '参数错误'));exit; 
            }
            $where = array(
                'menu_id' => $post['menu_id']
            );
            $child = Db::table('admin_menu')->where(array('pid' => $post['menu_id']))->count();
            if ($child) {
                echo json_encode(array('error' => 1,'msg' => '该菜单下有子菜单，无法删除'));exit;
            }
            $result = Db::table('admin_menu')->where($where)->delete();
            if ($result) {
                //记录log日志
                $this->action_log('删除了菜单，ID#'.$post['menu_id']);
                echo json_encode(array('error' => 0,'msg' => '删除成功'));exit;
            }else{
                echo json_encode(array('error' => 1,'msg' => '删除失败'));exit;
            }
            
        }
    }

    //菜单状态
    public function menu_status(){
        if (Request::instance()->isPost()) {
            $post = $_POST;
            if (empty($post['status']) || empty($post['menu_id'])) {
                echo json_encode(array('error' => 1,'msg' => '参数错误'));exit; 
            }
            $where = array(
                'menu_id|pid' => $post['menu_id']
            );

            if ($post['status'] == 1) {
                $str = '启用';
            }else{
                $str = '禁用';
            }
            
            $pid = Db::table('admin_menu')->where(array('menu_id' => $post['menu_id']))->value('pid');
            $pid_status = Db::table('admin_menu')->where(array('menu_id' => $pid))->value('status');
            if ($pid != 0 && $pid_status != 1) {
                $res = Db::table('admin_menu')->where(array('menu_id' => $pid))->setField('status',$post['status']);
                if (!$res) {
                    echo json_encode(array('error' => 1,'msg' => $str.'失败'));exit;
                }
            }
            $result = Db::table('admin_menu')->where($where)->setField('status',$post['status']);
            if ($result) {
                //记录log日志
                $this->action_log('修改了菜单状态为'.$post['status'].'，菜单数据是#'.serialize($where));
                echo json_encode(array('error' => 0,'msg' => $str.'成功'));exit;
            }else{
                echo json_encode(array('error' => 1,'msg' => $str.'失败'));exit;
            }
            
        }
    }

}
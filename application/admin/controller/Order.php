<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Log;
use think\Request;
use think\Config;

class Order extends Common {

    //订单列表
    public function lists() {
        
        $where = '1=1';

        if(isset($_GET['order_code']) && !empty($_GET['order_code'])){

            $where .= ' and order_code like "%'.$_GET['order_code'].'%"';
        }

        if(isset($_GET['status']) && $_GET['status'] != 'all'){

            $where .= ' and status = '.$_GET['status'].'';
        }

        $count = Db::table('order')->where($where)->count();

        $step = 10;

        $res = Db::table('order')->where($where)->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $status_arr = ['0' => '未支付','1' => '已支付','2' => '已发货','3' => '已收货'];
        
        foreach ($new_res['data'] as $key => $value) {
            
            $new_res['data'][$key]['str_status'] = $status_arr[$value['status']];
            
            $new_res['data'][$key]['user_name'] = Db::table('user')->where(['user_id' => $value['user_id']])->value('user_name');
        }

        $this->assign(['res' => $new_res['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);
        return $this->fetch();
    }

    //订单详情
    public function info() {
        
        $where = '1=1';

        if(!isset($_GET['order_id']) || empty($_GET['order_id'])){

            echo '<script>alert("参数错误");history.back(-1);</script>';exit;
        }

        $where .= ' and order_id = '.$_GET['order_id'];
        
        $res = Db::table('order')->where($where)->find();

        $status_arr = ['0' => '未支付','1' => '已支付','2' => '已发货','3' => '已收货'];

            
        $res['str_status'] = $status_arr[$res['status']];
            
        $user_info = Db::table('user')->field('user_name,user_img')->where(['user_id' => $res['user_id']])->find();

        $res['user_y_img'] = $user_info['user_img'];

        $res['user_y_name'] = $user_info['user_name'];

        $goods_info = Db::table('order_goods')->where(['order_id' => $res['order_id']])->select();

        $this->assign(['res' => $res]);
        $this->assign(['goods_info' => $goods_info]);
        return $this->fetch();
    }

    //确认已发货
    public function is_fa(){

        if(!isset($_POST['order_id']) || empty($_POST['order_id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $order_id = $_POST['order_id'];

        $result = Db::table('order')->where(['order_id' => $order_id])->update(['status' => 2]);

        if ($result) {
            
            echo (json_encode(['error' => 0,'msg' => '“已发货”成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '“已发货”失败']));exit;
        }
    }

    //添加备注
    public function remark(){

        if(!isset($_POST['order_id']) || empty($_POST['order_id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        if(!isset($_POST['remark']) || empty($_POST['remark'])){

            echo (json_encode(['error' => 1,'msg' => '备注不能为空']));exit;
        }

        $order_id = $_POST['order_id'];

        $result = Db::table('order')->where(['order_id' => $order_id])->update(['remark' => $_POST['remark']]);

        if (is_int($result)) {
            
            echo (json_encode(['error' => 0,'msg' => '添加备注成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '添加备注失败']));exit;
        }
    }

    //反馈列表
    public function fk_list(){

        $where = '1=1';

        if(isset($_GET['is_read']) && !empty($_GET['is_read'])){

                $where .= ' and is_read = '.($_GET['is_read'] - 1);
        }

        if(isset($_GET['order_code']) && !empty($_GET['order_code'])){

            $where .= ' and order_code like "%'.$_GET['order_code'].'%"';
        }

        $count = Db::table('user_fk')->where($where)->count();

        $step = 10;

        $res = Db::table('user_fk')->where($where)->paginate($step,$count,['query'=>request()->param()]);

        $page = $res->render();

        $new_res = $this->objToarray($res);

        $this->assign(['res' => $new_res['data'],'count' => $count,'page' => array('page' => $page,'firstRow' => (($new_res['current_page'] - 1) * $step))]);
        return $this->fetch();
    }

    //反馈处理
    public function read_fk(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        $id = $_POST['id'];

        $result = Db::table('user_fk')->where(['id' => $id])->update(['is_read' => 1]);

        if ($result) {
            
            echo (json_encode(['error' => 0,'msg' => '标记为“已处理”成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '标记为“已处理”失败']));exit;
        }
    }

    //添加反馈备注
    public function fk_remark(){

        if(!isset($_POST['id']) || empty($_POST['id'])){

            echo (json_encode(['error' => 1,'msg' => '参数错误']));exit;
        }

        if(!isset($_POST['remark']) || empty($_POST['remark'])){

            echo (json_encode(['error' => 1,'msg' => '备注不能为空']));exit;
        }

        $id = $_POST['id'];

        $result = Db::table('user_fk')->where(['id' => $id])->update(['remark' => $_POST['remark']]);

        if (is_int($result)) {
            
            echo (json_encode(['error' => 0,'msg' => '添加备注成功']));exit;
        }else{

            echo (json_encode(['error' => 1,'msg' => '添加备注失败']));exit;
        }
    }
}